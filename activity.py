# Accept a year input from the user and determine if it is a leap year or not.

# Add a validation for the leap year input:
#   Strings are not allowed for inputs
#   No zero or negative values

# input year
# validate
# logic
while True :
    year = input("Enter a year: ");
    try :
        intYear = int(year);    
    except :
        print("Strings are not allowed.");
        continue
    if intYear < 1 :
        print("No zero or negative values.");
        continue
    break
# print(type(intYear));
if (intYear % 4 == 0 and intYear % 100 != 0 or intYear % 400 == 0) :
    print(f"Year {intYear} is a Leap Year");
else :
    print(f"Year {intYear} is not a Leap Year");

# Accept two numbers (row and col) from the user and create a grid of asterisks using the two numbers (row and col).
row = input("Input number of Rows: ");
col = input("Input number of Columns: ");
row = int(row);
col = int(col);
# print(type(row));
# print(type(col));
for rowCount in range(row) :
    for colCount in range(col) :
        print("*", end = "");
    print("\n");

